import * as React from 'react';
import * as style from './style.css';

export namespace HomePage {
	export interface Props {
	}
	export interface State {
	}
}

export const HomePage: React.FC<HomePage.Props> = (props: HomePage.Props) => {
	const message: string = 'Hello';
	return (
		<main>
			{message} <span className={style.red}>Home Page</span>
		</main>
	);
};
